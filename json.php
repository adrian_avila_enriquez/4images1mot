<?php
	$result = array(
		"beginner" => array(
			array(
				"answer" => "ARBRE",
				"hint" => "A _ _ _ _",
				"img1" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427210873/games/4images1mot/A1/arbre/branche.jpg",
				"img2" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427210870/games/4images1mot/A1/arbre/feuille.jpg",
				"img3" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427210877/games/4images1mot/A1/arbre/racine.jpg",
				"img4" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427210876/games/4images1mot/A1/arbre/tronc.jpg"
				),
			array(
				"answer" => "CHAMBRE",
				"hint" => "C _ _ _ _ _ _",
				"img1" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427210976/games/4images1mot/A1/chambre/armoire.png",
				"img2" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427210983/games/4images1mot/A1/chambre/%C3%A9tag%C3%A8re.png",
				"img3" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427210987/games/4images1mot/A1/chambre/lampedechevet.png",
				"img4" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427210973/games/4images1mot/A1/chambre/lit.png"
			),
			array(
				"answer" => "CUISINE",
				"hint" => "C _ _ _ _ _ _",
				"img1" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427211093/games/4images1mot/A1/cuisine/casserole.png",
				"img2" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427211089/games/4images1mot/A1/cuisine/couteau.png",
				"img3" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427211090/games/4images1mot/A1/cuisine/cuisini%C3%A8re.png",
				"img4" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427211098/games/4images1mot/A1/cuisine/%C3%A9chelle.png"
			),
			array(
				"answer" => "DANSE",
				"hint" => "D _ _ _ _",
				"img1" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427211237/games/4images1mot/A1/danse/ballet.jpg",
				"img2" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427211247/games/4images1mot/A1/danse/breakdance.jpg",
				"img3" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427211247/games/4images1mot/A1/danse/samba.jpg",
				"img4" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427211245/games/4images1mot/A1/danse/tango.jpg"
			)
		),
		"intermediate" => array(
			array(
				"answer" => "AMOUR",
				"hint" => "La Saint Valentin, fête de l’...",
				"img1" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427295121/games/4images1mot/A2/Amour/anneaux.png",
				"img2" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427295122/games/4images1mot/A2/Amour/couple.png",
				"img3" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427295122/games/4images1mot/A2/Amour/lettred_amour.png",
				"img4" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427295122/games/4images1mot/A2/Amour/rose.png"
				),
			array(
				"answer" => "ANIMAL",
				"hint" => "Être vivant. Exemple : chien, chat, araignée...",
				"img1" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427294889/games/4images1mot/A2/animal/chat.png",
				"img2" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427294889/games/4images1mot/A2/animal/chien.png",
				"img3" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427294889/games/4images1mot/A2/animal/%C3%A9l%C3%A9phant.png",
				"img4" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427294889/games/4images1mot/A2/animal/giraffe.png"
				),
			array(
				"answer" => "ART",
				"hint" => "Exemple : la peinture, la musique, la littérature...",
				"img1" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299577/games/4images1mot/A2/art/danse.jpg",
				"img2" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299578/games/4images1mot/A2/art/exposition.jpg",
				"img3" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299577/games/4images1mot/A2/art/peinture.jpg",
				"img4" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299577/games/4images1mot/A2/art/sculpture.jpg"
				),
			array(
				"answer" => "BIJOUX",
				"hint" => "Exemple : bague, collier, bracelet...",
				"img1" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299650/games/4images1mot/A2/bijoux/anneau.jpg",
				"img2" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299650/games/4images1mot/A2/bijoux/bouclesd_oreilles.jpg",
				"img3" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299650/games/4images1mot/A2/bijoux/bracelet.jpg",
				"img4" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299650/games/4images1mot/A2/bijoux/collier.jpg"
				)
		),
		"advanced" => array(
			array(			
				"answer" => "AGRUME",
				"hint" => "Nom générique donné aux fruits tels que l'orange, le citron, le pamplemousse.",
				"img1" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427295430/games/4images1mot/B1/agrume/citron.jpg",
				"img2" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427295430/games/4images1mot/B1/agrume/limon.jpg",
				"img3" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427295430/games/4images1mot/B1/agrume/mandarine.jpg",
				"img4" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427295430/games/4images1mot/B1/agrume/orange.jpg"
				),
			array(
				"answer" => "BANQUE",
				"hint" => "Etablissement financier.",
				"img1" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299986/games/4images1mot/B1/banque/billets.jpg",
				"img2" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299986/games/4images1mot/B1/banque/cartebancaire.jpg",
				"img3" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299986/games/4images1mot/B1/banque/gratte-ciel.jpg",
				"img4" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299986/games/4images1mot/B1/banque/or.jpg"
				),
			array(
				"answer" => "AILES",
				"hint" => "Partie plane d’un avion et de certains véhicules volants qui permet la sustension.",
				"img1" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299255/games/4images1mot/B1/ailes/ange.jpg",
				"img2" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299255/games/4images1mot/B1/ailes/avion.jpg",
				"img3" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299255/games/4images1mot/B1/ailes/cygne.jpg",
				"img4" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299255/games/4images1mot/B1/ailes/papillon.jpg"
				),
			array(
				"answer" => "ARMES",
				"hint" => "Moyen de lutte pour combattre un adversaire.",
				"img1" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299309/games/4images1mot/B1/arme/canon.png",
				"img2" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299309/games/4images1mot/B1/arme/%C3%A9p%C3%A9e.png",
				"img3" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299308/games/4images1mot/B1/arme/fusil.png",
				"img4" => "http://res.cloudinary.com/azurmedia/image/upload/c_scale,h_160,w_180/v1427299309/games/4images1mot/B1/arme/pistolet.png"
				)
		)
	);

	header('Content-Type: application/json');
	echo json_encode($result);
?>