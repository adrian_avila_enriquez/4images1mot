# Game 4images1mot #

### Description. ###

Basic game for practicing the French language. The user sees 4 pictures and deduces the word in french which semantically relates the pictures. The complexity of the words is related to the french level (each level has 4 rounds).


### Resources. ###

* PHP
* Javascript
* CSS Framework : Foundation.
* Image Framework : Cloudinary
* Handlebarjs


### Game sections. ###

The game contains 5 main areas:

1) Navigation bar : Here you can find the controls for managing the game.

2) Level menu : It appears before starting a new round. Allows you to choose the complexity of the words.

3) Pictures panel : This sections contains the 4 pictures.

4) Keyboard : This sections has randomly located letters. 


### About the data. ###

#IMPORTANT#
The most important thing to know is that the everything is loaded from a json file. If you understand the json structure of each round, you will understand the game. 

data { frenchLevel { round { answer, hint, img_url_1, img_url_2, img_url_3, img_url_4 } } }

The pictures are retrieved using their https url, which can be found in the cloudinary account.


### Game logic. ###

The game works as the following cicle: Extract data from json -> Render game -> Inyect dynamic content -> Present game rounds.

1) First, we load the json data and we create a DataStructure with its content. In time, we will loop the DS in order to retrieve the elements of a game round answer (hint, img_url_1, img_url_2, img_url_3, img_url_4)

2) We pop up the level menu. 

3) Once we know the selected level, we use it to loop through the DS and get the elements of the respective game round. Immediatly after, we prepare everything to be presented...render the game, randomize the letters of the keyboard and inyect the images_url in the Pictures panel (handlebarjs documentation will help understand this process http://handlebarsjs.com/reference.html).

4) Once we finish a round, we start over with the next object in the DS. Once we finish a level, we can pop up the menu again or redirect the page to another site.